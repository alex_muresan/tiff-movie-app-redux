import React from "react";
import PropTypes from "prop-types";
import Watchlist from "../watchlist/watchlist";
import "./topBar.css";

class TopBar extends React.Component {
    constructor() {
        super();
        this.key = 0;
        this.buildMovies = this.buildMovies.bind( this );
    }

    buildMovies( movie ) {
        const baseUrl = "http://img.youtube.com/vi/";
        const thumbnail = "/hqdefault.jpg";
        const imageSource = movie.ytUrl === "" ?
                "https://upload.wikimedia.org/wikipedia/commons/6/64/Poster_not_available.jpg" :
                `${ baseUrl + movie.ytUrl.substring( 30 ) + thumbnail }`;
        return (
            <li key={ movie.id }>
                <div className="watchlist-movie">
                    <div
                        className="movie-poster"
                        onClick={ () => {
                            this.props.getMovie( movie.id );
                            this.props.onClickMovie();
                        } }
                    >
                        <img src={ imageSource } alt={ movie.titleRo } />
                    </div>
                    <div
                        className="movie-details"
                        onClick={ () => {
                            this.props.getMovie( movie.id );
                            this.props.onClickMovie();
                        } }
                    >
                        <span className="movie-title">{ movie.titleRo }</span>
                        <span>{ movie.year }</span>
                    </div>
                </div>
            </li>
        );
    }

    render() {
        return (
            <div className={ `navbar ${ this.props.changeView } ${ this.props.displayWatchlist }` }>
                <button className="button-list" onClick={ this.props.handleChangeView }>
                    <span className="list">List</span>
                    <span className="grid">Grid</span>
                </button>
                <Watchlist
                    showWatchlist={ this.props.showWatchlist }
                    watchlist={ this.props.watchlist }
                    removeFromWatchlist={ this.props.removeFromWatchlist }
                    onClickMovie={ this.props.onClickMovie }
                    getMovie={ this.props.getMovie }
                    removeEmpty={ this.props.removeEmpty }
                />
                <input type="text" className="search-bar" placeholder="Search" onChange={ this.props.searchInputHandler } />
                <div className={ `search-movies ${ this.props.displaySearchMovies }` }>
                    <ul>
                        {
                            this.props.searchlist.map( this.buildMovies )
                        }
                    </ul>
                </div>
            </div>
        );
    }
}
TopBar.propTypes = {
    handleChangeView: PropTypes.func.isRequired,
    showWatchlist: PropTypes.func.isRequired,
    removeFromWatchlist: PropTypes.func.isRequired,
    onClickMovie: PropTypes.func.isRequired,
    getMovie: PropTypes.func.isRequired,
    changeView: PropTypes.string.isRequired,
    removeEmpty: PropTypes.string.isRequired,
    displayWatchlist: PropTypes.string.isRequired,
    displaySearchMovies: PropTypes.string.isRequired,
    searchInputHandler: PropTypes.func.isRequired,
    searchlist: PropTypes.arrayOf( PropTypes.shape( {
        titleRo: PropTypes.string,
        year: PropTypes.string,
        country: PropTypes.string,
        ytUrl: PropTypes.string,
    } ) ).isRequired,
    watchlist: PropTypes.arrayOf( PropTypes.shape( {
        titleRo: PropTypes.string,
        year: PropTypes.string,
        country: PropTypes.string,
        ytUrl: PropTypes.string,
    } ) ).isRequired,
};

export default TopBar;
